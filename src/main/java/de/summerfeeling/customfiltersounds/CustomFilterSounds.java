package de.summerfeeling.customfiltersounds;

import de.summerfeeling.customfiltersounds.reflect.MethodAccessor;
import de.summerfeeling.customfiltersounds.reflect.Reflect;
import net.labymod.api.LabyModAddon;
import net.labymod.ingamechat.tabs.GuiChatFilter;
import net.labymod.main.LabyMod;
import net.labymod.settings.elements.SettingsElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.audio.SoundList;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.util.ResourceLocation;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CustomFilterSounds extends LabyModAddon {
	
	@Override
	public void onEnable() {
		List<String> soundNames = Reflect.getField(GuiChatFilter.class, "soundNames").getStatic();
		MethodAccessor soundMap = Reflect.getMethod(SoundHandler.class, LabyMod.isForge() ? "func_175085_a" : "a", InputStream.class);
		
		((IReloadableResourceManager) Minecraft.getMinecraft().getResourceManager()).registerReloadListener(new IResourceManagerReloadListener() {
			public void onResourceManagerReload(IResourceManager manager) {
				soundNames.clear();
				
				for (String domains : manager.getResourceDomains()) {
					boolean defaultDomain = domains.equals("minecraft");
					
					try {
						for (IResource resource : manager.getAllResources(new ResourceLocation(domains, "sounds.json"))) {
							Map<String, SoundList> sounds = soundMap.invoke(Minecraft.getMinecraft().getSoundHandler(), resource.getInputStream());
							
							for (Entry<String, SoundList> sound : sounds.entrySet()) {
								String soundName = (!defaultDomain ? domains + ":" : "") + sound.getKey();
								
								if (!soundNames.contains(soundName)) {
									soundNames.add(soundName);
								}
							}
						}
					} catch (IOException ignored) { }
				}
				
				System.out.println("[CustomFilterSounds] Successfully loaded " + soundNames.size() + " available sounds.");
			}
		});
	}
	
	public void onDisable() { }
	public void loadConfig() { }
	protected void fillSettings(List<SettingsElement> list) { }
}
